#!/bin/sh

# set ssh port
echo "Please choose a port for SSH"
read ssh_port
sed -i "s/^#*Port .*$/Port $ssh_port/" /etc/ssh/sshd_config
sed -i "s/#PermitRootLogin .*$/PermitRootLogin yes/" /etc/ssh/sshd_config
sed -i "s/#PasswordAuthentication .*$/PasswordAuthentication yes/" /etc/ssh/sshd_config
systemctl restart sshd

# show ssh ed25519 fingerprint
ssh-keygen -l -f /etc/ssh/ssh_host_ed25519_key.pub

#install python
pacman -S python
